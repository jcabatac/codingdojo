export class TicTacToe {
  player1: string = "X";
  player2: string = "O";
  board: string[][];
  currentPlayer: string;
  moveLength: number = 0;

  public initializeGame(): string[][] {
    this.board = [
      ["", "", ""],
      ["", "", ""],
      ["", "", ""],
    ];
    this.currentPlayer = this.player1;
    return this.board;
  }

  makeMove(row: number, col: number): boolean {
    if (this.board[row][col] === "") {
      this.board[row][col] = this.currentPlayer;
      this.currentPlayer =
        this.currentPlayer === this.player1 ? this.player2 : this.player1;
      this.moveLength++;
      console.table(this.board);
      return true;
    }
    return false;
  }

  isBoardFull(): boolean {
    return this.board.every((row) => row.every((cell) => cell !== ""));
  }

  checkIfOccupied(row: number, col: number): boolean {
    if (this.board[row][col] === "X" || this.board[row][col] === "O") {
      return true;
    } else {
      return false;
    }
  }

  checkWin(player: string): boolean {
    // Check rows for a win
    for (let row = 0; row < 3; row++) {
      if (
        this.board[row][0] === player &&
        this.board[row][1] === player &&
        this.board[row][2] === player
      ) {
        return true;
      }
    }

    // Check columns for a win
    for (let col = 0; col < 3; col++) {
      if (
        this.board[0][col] === player &&
        this.board[1][col] === player &&
        this.board[2][col] === player
      ) {
        return true;
      }
    }

    // Check diagonals for a win
    if (
      (this.board[0][0] === player &&
        this.board[1][1] === player &&
        this.board[2][2] === player) ||
      (this.board[0][2] === player &&
        this.board[1][1] === player &&
        this.board[2][0] === player)
    ) {
      return true;
    }

    return false;
  }
}
// module.exports = TicTacToe;
